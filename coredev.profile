<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function coredev_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#value'] = $_SERVER['SERVER_NAME'];
  $form['site_information']['site_mail']['#value'] = 'coredev@example.com';
  $form['admin_account']['account']['mail']['#value'] = 'coredev@example.com';
  $form['site_information']['site_name']['#access'] = FALSE;
  $form['site_information']['site_mail']['#access'] = FALSE;
  $form['admin_account']['account']['mail']['#access'] = FALSE;
}
